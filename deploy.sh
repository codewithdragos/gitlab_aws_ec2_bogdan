# install node.js
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install --lts

# clean up old app
pm2 kill
npm remove pm2 -g

# installing dependencies
echo "Installing pm2"
npm install pm2 -g
echo "Running npm install"
npm install

# start app with pm2
echo "Running the app"
npm run start:production