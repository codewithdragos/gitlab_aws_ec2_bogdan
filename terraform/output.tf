output "instance_ip_addr" {
  value = aws_instance.web.private_ip
  description = "The private IP address of the main server instance."
}

output "instance_dns" {
  value = aws_instance.web.public_dns
}

output "private_key" {
  value = tls_private_key.deploy.private_key_pem
  sensitive = true
}